package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int numRows;
    private int numCols;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.numRows = rows;
        this.numCols = columns;
        grid = new CellState[numRows][numCols];

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                grid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return numRows;
    }

    @Override
    public int numColumns() {
        return numCols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row >= numRows || column < 0 || column >= numCols) {
            throw new IndexOutOfBoundsException();
        }
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row >= numRows || column < 0 || column >= numCols) {
            throw new IndexOutOfBoundsException();
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid copy = new CellGrid(numRows, numCols, CellState.DEAD);

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                copy.set(i, j, this.get(i, j));
            }
        }
        return copy;
    }
    
}
