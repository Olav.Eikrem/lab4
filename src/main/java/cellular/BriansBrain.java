package cellular;

import datastructure.CellGrid;

public class BriansBrain extends GameOfLife implements CellAutomaton {

    public BriansBrain(int rows, int columns) {
        super(rows, columns);
//        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
//        initializeCells();
    }

    @Override
    public CellState getNextCell(int row, int col) {

        if (getCellState(row, col) == CellState.ALIVE) return CellState.DYING;
        if (getCellState(row, col) == CellState.DYING) return CellState.DEAD;

        int aliveNeighbour = super.countNeighbors(row, col, CellState.ALIVE);

        if(aliveNeighbour == 2) return CellState.ALIVE;

        return CellState.DEAD;
    }
}
